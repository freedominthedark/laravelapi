<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::where('status', NULL)->get();

        return response()->json([
            'status' => true,
            'categorys' => $category
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $category = Category::create($request->all());

        return response()->json([
            'status' => true,
            'message' => "Category Created successfully!",
            'category' => $category
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProductRequest $request, Category $category)
    {
        $category->update($request->all());

        return response()->json([
            'status' => true,
            'message' => "Category Updated successfully!",
            'category' => $category
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //$data = DB::table('products')->whereBetween('price', [101, 200])

        /*$data = DB::table('categories')->where('categories.id', $category->id)
            ->leftJoin('product_categories','categories.id','=','product_categories.id_category')
            ->leftJoin('products','products.id','=','product_categories.id_product')
            ->select('products.title as title',
                'products.price as price',
                'products.description as description',
                'categories.title as title')
            ->orderBy('products.price')
            ->get();*/
        $data = DB::table('product_categories')->where('product_categories.id_category', $category->id)
            ->leftJoin('products','products.id','=','product_categories.id_product')
            ->select('products.title as title',
                'products.price as price',
                'products.description as description')
            ->orderBy('products.price')
            ->get();

        //$product = ProductCategory::where('id_category', $category->id)->get();
        //if ($data == NULL){
        if (!isset($data[0])){
            $category->delete();

            return response()->json([
                'status' => true,
                'message' => "Category Deleted successfully!",
                'category_id' => $category->id,
                'data' => $data
            ], 200);
        }else{
            return response()->json([
                'status' => false,
                'message' => "Category NOT Deleted! Еhe category exists in the product.",
                'category_id' => $category->id,
                //'product' => $product,
                'data' => $data,
            ], 200);
        }
    }
}
