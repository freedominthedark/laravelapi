<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    use ExceptionTrait;

    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception|Throwable $exception)
    {
        return $this->checkValidationException($request, $exception);
    }

    /**
     * Check for validation exception.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $e
     *
     * @return mixed
     */
    private function checkValidationException($request, $e)
    {
        if ($e instanceof ValidationException && $request->is(RouteServiceProvider::SSO_PATTERN)) {
            return response()->json([
                'code' => $e->status,
                'message' => $e->getMessage(),
                'errors' => $e->errors()
            ], $e->status);
        } elseif ($e instanceof TokenExpiredException) {
            return response()->json(['status' => false, 'message' => 'Token is Expired'], Response::HTTP_UNAUTHORIZED);
        } elseif ($e instanceof TokenInvalidException) {
            return response()->json(['status' => false, 'message' => 'Token is Invalid'], Response::HTTP_UNAUTHORIZED);
        } elseif ($e instanceof JWTException) {
            return response()->json(['status' => false, 'message' => 'Unauthenticated.'], Response::HTTP_UNAUTHORIZED);
        } elseif ($e instanceof ModelNotFoundException) {
            return response()->json(['status' => false, 'message' => 'ID not found.'],Response::HTTP_NOT_FOUND);
        }

        return $this->notFoundHttpExceptionHandler($request, $e);
    }

    /**
     * NotFoundHttpException handler.
     *
     * @param $request
     * @param $e
     *
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    private function notFoundHttpExceptionHandler($request, $e)
    {
        if ($e instanceof ModelNotFoundException) {
            return response()->json(['status' => false, 'message' => 'Not Found!'], Response::HTTP_NOT_FOUND);
        }

        return $this->authorizationExceptionHandler($request, $e);
    }

    /**
     * authorizationExceptionHandler handler.
     *
     * @param $request
     * @param $e
     *
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    private function authorizationExceptionHandler($request, $e)
    {
        if ($e instanceof AuthorizationException) {
            return response()->json(['status' => false, 'message' => 'Not Found!'], Response::HTTP_NOT_FOUND);
        }

        return parent::render($request, $e);
    }
}
