<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductController;
use \App\Http\Controllers\Api\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// https://docs.google.com/document/d/1oSTzvjFFYXWqMoCtP5yj0o0cqYAGk-xTlYDuw7XitRY/edit
// http://laravel.test/
// https://gitlab.com/freedominthedark/laravelapi

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// ALL API
Route::apiResource('products', ProductController::class);
Route::apiResource('category', CategoryController::class);
